package com.example.tvshowapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "tv_show.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_TV_SHOW = "tv_show";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_SHOW_NAME = "show_name";
    public static final String COLUMN_CHANNEL = "channel";
    public static final String COLUMN_LANGUAGE = "language";
    public static final String COLUMN_GENRE = "genre";

    private static final String CREATE_TABLE_TV_SHOW =
            "CREATE TABLE " + TABLE_TV_SHOW + " (" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_SHOW_NAME + " TEXT NOT NULL, " +
                    COLUMN_CHANNEL + " TEXT NOT NULL, " +
                    COLUMN_LANGUAGE + " TEXT NOT NULL, " +
                    COLUMN_GENRE + " TEXT NOT NULL)";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_TV_SHOW);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TV_SHOW);
        onCreate(db);
    }
}
