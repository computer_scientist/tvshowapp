package com.example.tvshowapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class TvShowDataSource {

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;

    public TvShowDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public long addTvShow(String showName, String channel, String language, String genre) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_SHOW_NAME, showName);
        values.put(DatabaseHelper.COLUMN_CHANNEL, channel);
        values.put(DatabaseHelper.COLUMN_LANGUAGE, language);
        values.put(DatabaseHelper.COLUMN_GENRE, genre);

        return database.insert(DatabaseHelper.TABLE_TV_SHOW, null, values);
    }

    public Cursor getAllTvShows() {
        String[] columns = {
                DatabaseHelper.COLUMN_ID,
                DatabaseHelper.COLUMN_SHOW_NAME,
                DatabaseHelper.COLUMN_CHANNEL,
                DatabaseHelper.COLUMN_LANGUAGE,
                DatabaseHelper.COLUMN_GENRE
        };

        return database.query(DatabaseHelper.TABLE_TV_SHOW, columns, null, null, null, null, null);
    }
}
