package com.example.tvshowapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

/**
 * This activity displays the TV Show information.
 * It allows the user to navigate back to the MainActivity.
 */
public class DisplayShowsActivity extends AppCompatActivity {
    private TvShowDataSource dataSource; // Database interface

    /**
     * This method is called when the activity is created.
     * @param savedInstanceState The saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Call the super class onCreate to complete the creation of activity like
        super.onCreate(savedInstanceState);

        // Set the user interface layout for this activity
        setContentView(R.layout.activity_display_shows);

        // Set the selected item in the BottomNavigationView
        setSelectedBottomNavigationItem(R.id.navigation_display);

        // Initialize the database interface
        dataSource = new TvShowDataSource(this);

        // Open the database interface
        dataSource.open();

        // Get all the TV Shows from the database
        Cursor cursor = dataSource.getAllTvShows();

        // Initialize the LinearLayout
        LinearLayout showsLinearLayout = findViewById(R.id.showsLinearLayout);

        // Iterate through the database results and create TextViews for each row
        while (cursor.moveToNext()) {
            // Get the TV Show information from the database
            @SuppressLint("Range") String showInfo = String.format("Show Name: %s\nChannel: %s\nLanguage: %s\nGenre: %s\n\n",
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_SHOW_NAME)),
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CHANNEL)),
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_LANGUAGE)),
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_GENRE)));

            // Create a TextView for the TV Show information
            TextView showInfoTextView = new TextView(this);

            // Set the TextView properties
            showInfoTextView.setText(showInfo);

            // Add the TextView to the LinearLayout
            showsLinearLayout.addView(showInfoTextView);
        }

        // Close the database cursor
        cursor.close();

        // Initialize the BottomNavigationView
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        // Set the selected item in the BottomNavigationView
        bottomNavigationView.setOnItemSelectedListener(item -> {
            // Get the id of the selected item
            int id = item.getItemId();

            // Check which item was selected
            if (id == R.id.navigation_edit) {
                // Navigate to the MainActivity
                Intent displayShowsIntent = new Intent(
                        getApplicationContext(), MainActivity.class);
                // Start the MainActivity
                startActivity(displayShowsIntent);
                // Override the transition animation
                overridePendingTransition(0, 0);
            } else if (id == R.id.navigation_display) {
                // Do nothing, we are already in Display mode
            }
            // Return true to display the selected item
            return true;
        });
    }

    /**
     * This method is called when the activity is resumed.
     */
    @Override
    protected void onResume() {
        dataSource.open();
        super.onResume();
    }

    /**
     * This method is called when the activity is paused.
     */
    @Override
    protected void onPause() {
        dataSource.close();
        super.onPause();
    }

    /**
     * This method sets the selected item in the BottomNavigationView.
     * @param itemId The id of the selected item
     */
    private void setSelectedBottomNavigationItem(int itemId) {
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.getMenu().findItem(itemId).setChecked(true);
    }
}

