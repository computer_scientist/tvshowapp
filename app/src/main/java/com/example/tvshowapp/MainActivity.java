package com.example.tvshowapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.material.bottomnavigation.BottomNavigationView;

/**
 * This is the main activity for the TV Show app.
 * It allows the user to enter TV Show information.
 * The user can also save the TV Show information to the database.
 * The user can also navigate to the DisplayShowsActivity to view the TV Show information.
 */
public class MainActivity extends AppCompatActivity {

    private EditText showNameEditText; // EditText for the TV Show name
    private EditText channelEditText; // EditText for the TV Show channel
    private EditText languageEditText; // EditText for the TV Show language
    private EditText genreEditText; // EditText for the TV Show genre
    private TvShowDataSource dataSource; // Database interface

    /**
     * This method is called when the activity is created.
     * @param savedInstanceState The saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Call the super class onCreate to complete the creation of activity like
        super.onCreate(savedInstanceState);

        // Set the user interface layout for this activity
        setContentView(R.layout.activity_main);

        // Set the selected item in the BottomNavigationView
        setSelectedBottomNavigationItem(R.id.navigation_edit);

        // Initialize the database interface
        dataSource = new TvShowDataSource(this);

        // Open the database interface
        dataSource.open();

        // Initialize the EditTexts
        showNameEditText = findViewById(R.id.showNameEditText);
        channelEditText = findViewById(R.id.channelEditText);
        languageEditText = findViewById(R.id.languageEditText);
        genreEditText = findViewById(R.id.genreEditText);

        // Initialize the ToggleButton
        ToggleButton toggleButton = findViewById(R.id.toggleButton);

        // Initialize the Save Button
        Button saveButton = findViewById(R.id.saveButton);

        // Set the OnCheckedChangeListener for the ToggleButton
        toggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            showNameEditText.setEnabled(isChecked);
            channelEditText.setEnabled(isChecked);
            languageEditText.setEnabled(isChecked);
            genreEditText.setEnabled(isChecked);
            // Set the visibility of the Save Button
            saveButton.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        });

        // Set the OnClickListener for the Save Button
        saveButton.setOnClickListener(v -> {
            // Get the text from the EditTexts
            String showName = showNameEditText.getText().toString();
            String channel = channelEditText.getText().toString();
            String language = languageEditText.getText().toString();
            String genre = genreEditText.getText().toString();

            // Add the TV Show to the database
            long result = dataSource.addTvShow(showName, channel, language, genre);

            // Display a toast message indicating whether or not the TV Show was saved
            if (result != -1) {
                Toast.makeText(this, "TV Show saved", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Error saving TV Show", Toast.LENGTH_SHORT).show();
            }

            // Clear the EditTexts
            showNameEditText.setText("");
            channelEditText.setText("");
            languageEditText.setText("");
            genreEditText.setText("");
        });

        // Initialize the BottomNavigationView
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        // Set the OnItemSelectedListener for the BottomNavigationView
        bottomNavigationView.setOnItemSelectedListener(item -> {
            // Get the id of the selected item
            int id = item.getItemId();

            // Check which item was selected
            if (id == R.id.navigation_edit) {
                return true;
            } else if (id == R.id.navigation_display) {
                // Create an Intent to navigate to the DisplayShowsActivity
                Intent displayShowsIntent = new Intent(
                        getApplicationContext(), DisplayShowsActivity.class);
                // Clear the activity stack
                displayShowsIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                // Start the DisplayShowsActivity
                startActivity(displayShowsIntent);
                // Disable the animation
                overridePendingTransition(0, 0);
                return true;
            }
            // Return true to display the item as the selected item
            return false;
        });
    }

    /**
     * This method is called when the activity is resumed.
     */
    @Override
    protected void onResume() {
        dataSource.open();
        super.onResume();
    }

    /**
     * This method is called when the activity is paused.
     */
    @Override
    protected void onPause() {
        dataSource.close();
        super.onPause();
    }

    /**
     * This method sets the selected item in the BottomNavigationView.
     * @param itemId The id of the selected item
     */
    private void setSelectedBottomNavigationItem(int itemId) {
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.getMenu().findItem(itemId).setChecked(true);
    }
}


